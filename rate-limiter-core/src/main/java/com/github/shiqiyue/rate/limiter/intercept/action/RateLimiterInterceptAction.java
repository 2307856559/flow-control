package com.github.shiqiyue.rate.limiter.intercept.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/****
 * 流量控制-拦截动作
 * 
 * @author wwy
 *
 */
public interface RateLimiterInterceptAction {
	
	/***
	 * 超过访问次数-执行的动作
	 * 
	 * @param request
	 * @param response
	 */
	public void action(HttpServletRequest request, HttpServletResponse response);
	
}
