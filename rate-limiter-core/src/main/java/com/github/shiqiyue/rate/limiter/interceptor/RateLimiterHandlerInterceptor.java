package com.github.shiqiyue.rate.limiter.interceptor;

import com.github.shiqiyue.rate.limiter.AbstractRateLimiterAction;
import com.github.shiqiyue.rate.limiter.RateLimiterConfigurer;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * 流量控制Interceptor
 * 
 * @author wwy
 *
 */
public class RateLimiterHandlerInterceptor extends AbstractRateLimiterAction implements AsyncHandlerInterceptor {
	
	public RateLimiterHandlerInterceptor(RateLimiterConfigurer configurer) {
		super(configurer);
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		return doRateLimit(request, response);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
	}
	
}
