package com.github.shiqiyue.flow.contol.sample.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 * redis配置
 * 
 * @author wwy
 *
 */
@Configuration
public class RedisConfig {
	
	@Bean(destroyMethod = "shutdown")
	public RedissonClient redissonClient() {
		Config config = new Config();
		config.useSingleServer().setAddress("redis://localhost:6379");
		RedissonClient redisson = Redisson.create(config);
		return redisson;
	}
}
